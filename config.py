import torch
import numpy as np
import albumentations as A

from albumentations.pytorch import ToTensorV2


# Предустановки
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
IMAGE_SIZE = 256
IN_CHANNELS = 3
NUM_RESIDUALS = 8
NUM_WORKERS = 2

# Обучение
NUM_EPOCHS = 512
BATCH_SIZE = 1
LEARNING_RATE = 3e-5
LOAD_MODEL = False
SAVE_MODEL = False
USE_TENSORBOARD = True

# Датасет
TRAIN_DIR = "dataset/train"
DATASET_MEAN = np.array([0.5, 0.5, 0.5])
DATASET_STD = np.array([0.5, 0.5, 0.5])
# DATASET_MEAN = np.array([0.485, 0.456, 0.406])
# DATASET_STD = np.array([0.229, 0.224, 0.225])

# Другое
CHECKPOINT_DIR = "checkpoints"
CHECKPOINT_MODEL = "checkpoint.pth.tar"


train_transforms = A.Compose(
    [
        A.Resize(width=IMAGE_SIZE, height=IMAGE_SIZE),
        A.HorizontalFlip(p=0.5),
        A.Rotate(limit=10, always_apply=True),
        A.Normalize(mean=DATASET_MEAN, std=DATASET_STD, max_pixel_value=255.0),
        ToTensorV2(),
     ],
    additional_targets={"image0": "image"},
)

test_transforms = A.Compose(
    [
        A.Resize(width=IMAGE_SIZE, height=IMAGE_SIZE),
        A.Normalize(mean=DATASET_MEAN, std=DATASET_STD, max_pixel_value=255.0),
        ToTensorV2(),
     ],
)
