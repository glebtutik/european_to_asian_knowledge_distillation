import time
import numpy as np
import torch.optim as optim

from PIL import Image
from torch.utils.tensorboard import SummaryWriter
from utils import *


def test(model, img_dir="dataset/test"):
    # Загружаем и подготавливаем картинки:
    images = [img for img in os.listdir(img_dir) if img.endswith(".png") or img.endswith(".jpg")]  # Names
    images = [Image.open(os.path.join(img_dir, img)).convert("RGB") for img in images]  # Names -> PIL Images
    images = [np.array(img) for img in images]  # PIL Images -> np.arrays
    images = [config.test_transforms(image=img)["image"] for img in images]  # Transforms
    images = [img.to(config.DEVICE) for img in images]

    # # Загружаем модели генераторов:
    # gen_E, gen_A = load_generators()

    # Генерируем изображения:
    prediction = [postprocessing(model(img.detach())) for img in images]

    images = [postprocessing(img.detach()) for img in images]

    # Собираем всё вместе:
    images = np.concatenate(images, axis=2)
    prediction = np.concatenate(prediction, axis=2)

    return np.concatenate((images, prediction), axis=1)


if __name__ == "__main__":
    writer = SummaryWriter()
    current = test()
    writer.add_image("Generated images", current, global_step=0)
    time.sleep(10)
