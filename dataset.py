import os
import config
import numpy as np

from PIL import Image
from torch.utils.data import Dataset, DataLoader


class EuropeanAsianDataset(Dataset):
    def __init__(self, root, transform=None):
        self.root = root
        self.transform = transform

        # Получаем списки имен изображений
        self.names = [name for name in os.listdir(root) if name.endswith(".jpg")]

        # Находим количество изображений
        self.dataset_len = len(self.names)

    def __len__(self):
        return self.dataset_len

    def __getitem__(self, index):
        # Получаем имя изображения
        name = self.names[index]

        # Получаем полный путь к изображению
        full_path = os.path.join(self.root, name)

        # Получаем изображение и конвертируем в np.array
        image = np.array(Image.open(full_path).convert("RGB"))

        # Применяем аугментации
        augmentations = self.transform(image=image[:, :256, :], image0=image[:, 256:, :])
        input_image = augmentations["image"]
        target_image = augmentations["image0"]

        return input_image, target_image


def test():
    dataset = EuropeanAsianDataset(
        root=config.TRAIN_DIR,
        transform=config.train_transforms
    )

    data_loader = DataLoader(
        dataset,
        batch_size=config.BATCH_SIZE,
        shuffle=True,
        num_workers=config.NUM_WORKERS,
        pin_memory=True
    )


if __name__ == "__main__":
    test()
