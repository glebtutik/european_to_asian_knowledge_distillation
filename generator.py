import torch
import torch.nn as nn
import config

from torchsummary import summary


class BlockDown(nn.Module):
    def __init__(self, in_channels, out_channels, **kwargs):
        super().__init__()
        self.block = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, padding_mode="zeros", **kwargs),
            nn.InstanceNorm2d(out_channels),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.block(x)


class BlockUp(nn.Module):
    def __init__(self, in_channels, out_channels, **kwargs):
        super().__init__()
        self.block = nn.Sequential(
            nn.ConvTranspose2d(in_channels, out_channels, **kwargs),
            nn.InstanceNorm2d(out_channels),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.block(x)


class Generator(nn.Module):
    def __init__(self, in_channels, num_features=64):
        super().__init__()
        self.initial = nn.Sequential(
            nn.Conv2d(in_channels, num_features, kernel_size=7, stride=1, padding="same", padding_mode="zeros"),
            nn.ReLU(inplace=True),
        )

        self.blocks = nn.ModuleList(
            [
                BlockDown(num_features * 1, num_features * 2, kernel_size=3, stride=2, padding=1),
                BlockDown(num_features * 2, num_features * 4, kernel_size=3, stride=2, padding=1),
                # BlockDown(num_features * 4, num_features * 4, kernel_size=3, stride=2, padding=1),
                # BlockUp(num_features * 4, num_features * 4, kernel_size=3, stride=2, padding=1, output_padding=1),
                BlockUp(num_features * 4, num_features * 2, kernel_size=3, stride=2, padding=1, output_padding=1),
                BlockUp(num_features * 2, num_features * 1, kernel_size=3, stride=2, padding=1, output_padding=1),
            ]
        )

        self.last = nn.Conv2d(num_features * 1, in_channels, kernel_size=7, stride=1, padding="same", padding_mode="zeros")

    def forward(self, x):
        x = self.initial(x)

        for layer in self.blocks:
            x = layer(x)

        x = self.last(x)

        x = torch.tanh(x)
        return x


def test():
    x = torch.randn((config.BATCH_SIZE, config.IN_CHANNELS, config.IMAGE_SIZE, config.IMAGE_SIZE))
    model = Generator(config.IN_CHANNELS)
    prediction = model(x)

    print("Input shape: ", x.shape)
    print("Output shape: ", prediction.shape)

    summary(model, depth=5)

    # print(model)


if __name__ == "__main__":
    test()
