import os
import time
import torch
import config
from model_test import test
import numpy as np
import torch.nn as nn
import torch.optim as optim

from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from generator import Generator
from discriminator import Discriminator
from dataset import EuropeanAsianDataset
from tqdm import tqdm
from utils import *


def train():
    # Инициализируем модель
    # model = Model(in_channels=config.IN_CHANNELS, num_residuals=config.NUM_RESIDUALS).to(config.DEVICE)
    gen_model = Generator(in_channels=config.IN_CHANNELS).to(config.DEVICE)
    disc_model = Discriminator(in_channels=config.IN_CHANNELS).to(config.DEVICE)
    # Инициализируем оптимизатор
    gen_opt = optim.Adam(
        params=list(gen_model.parameters()),
        lr=config.LEARNING_RATE,
        betas=(0.5, 0.999),
    )

    disc_opt = optim.Adam(
        params=list(disc_model.parameters()),
        lr=config.LEARNING_RATE,
        betas=(0.5, 0.999),
    )

    # Загружаем датасет
    dataset = EuropeanAsianDataset(
        root=config.TRAIN_DIR,
        transform=config.train_transforms,
    )

    data_loader = DataLoader(
        dataset,
        batch_size=config.BATCH_SIZE,
        shuffle=True,
        num_workers=config.NUM_WORKERS,
        pin_memory=True
    )

    # # Загружаем последний чекпоинт
    # if config.LOAD_MODEL:
    #     print("\033[32m{}".format("=> Загрузка последнего чекпоинта"))
    #     load_checkpoint(gen_model, gen_opt, config.LEARNING_RATE, get_last_checkpoint(config.CHECKPOINT_MODEL)) # ????

    MSE = nn.MSELoss()

    scaler = torch.cuda.amp.GradScaler()

    writer = SummaryWriter()

    # ----- Цикл обучения ----- #
    for epoch in range(config.NUM_EPOCHS):

        loop = tqdm(data_loader)
        for idx, (input_image, target_image) in enumerate(loop):

            input_image = input_image.to(config.DEVICE)
            target_image = target_image.to(config.DEVICE)

            # Train Discriminator max log(D(x)) + log(1 - D(G(z)))
            fake_image = gen_model(input_image)
            disc_real = disc_model(target_image)
            loss_disc_real = MSE(disc_real, torch.ones_like(disc_real))
            disc_fake = disc_model(fake_image.detach())
            loss_disc_fake = MSE(disc_fake, torch.zeros_like(disc_fake))
            loss_disc = loss_disc_real + loss_disc_fake

            disc_model.zero_grad()
            loss_disc.backward()
            disc_opt.step()

            # Train Generator: min log(1 - D(G(z))) <-> max log(D(G(z))
            output = disc_model(fake_image)
            loss_gen = MSE(output, torch.ones_like(output))
            gen_model.zero_grad()
            loss_gen.backward()
            gen_opt.step()

            if config.USE_TENSORBOARD and idx % 200 == 0:
                generated_image = postprocessing(fake_image)
                current_images = np.concatenate((postprocessing(input_image.detach()), postprocessing(target_image.detach()), generated_image), axis=2)
                writer.add_image(f"Current images", current_images, global_step=int(time.time()))

        # Сохраняем модели
        # if config.SAVE_MODEL and epoch % 10 == 0:
        #     print("\033[32m{}".format("=> Сохранение чекпоинта"))

            # # Создаем директорию для сохранения
            # save_dir = os.path.join(config.CHECKPOINT_DIR, get_current_time())
            # make_directory(save_dir)
            #
            # # Сохраняем
            # save_checkpoint(model, opt, os.path.join(save_dir, config.CHECKPOINT_MODEL))

        # Обновляем tensorboard
        if config.USE_TENSORBOARD:
            writer.add_image("Generated images", test(gen_model), global_step=epoch)


if __name__ == "__main__":
    train()
